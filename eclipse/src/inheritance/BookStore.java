package inheritance;

public class BookStore {
	public static void main(String[] args) {
		Book[] shelf = new Book[5];
		shelf[0] = new Book("Author One","Just a book");
		shelf[2] = new Book("Author Two","Just a book sequel");
		shelf[1] = new ElectronicBook(10,"eAuthor One","Book with an e 1");
		shelf[3] = new ElectronicBook(20,"eAuthor Two","Book with an e 2");
		shelf[4] = new ElectronicBook(30,"eAuthor Three","Book with an e 3");
		
		for(Book b : shelf) {
			System.out.println(b + "\n");
		};
	}
}
