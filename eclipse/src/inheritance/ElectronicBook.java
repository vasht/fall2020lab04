package inheritance;

public class ElectronicBook extends Book{
	private double numberBytes;
	
	public ElectronicBook(double numberBytes,String author,String title) {
		super(author,title);
		this.numberBytes = numberBytes;
	}
	public String toString() {
		return super.toString() + "\n Size: " + numberBytes;
	}
}
