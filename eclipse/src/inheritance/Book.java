package inheritance;

public class Book {
	protected String title;
	private String author;
	
	public Book(String author,String title) {
		this.title = title;
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public String getAuthor() {
		return author;
	}
	public String toString() {
		return "Author: " + author + "\nBook:  " + title;
	}
}
