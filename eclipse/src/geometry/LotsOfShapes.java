package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Circle(5);
		shapes[1] = new Circle(10);
		shapes[2] = new Rectangle(2,3);
		shapes[3] = new Rectangle(6,7);
		shapes[4] = new Square(20);
		
		for(Shape s : shapes) {
			System.out.println("Perimeter: " + s.getPerimeter());
			System.out.println("Area: " + s.getArea());
			System.out.println();
		}
	}
}
