package geometry;

public interface Shape {
	public double getArea();
	public double getPerimeter();
}
